console.log("Hello World!");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printBio() {		
		let fullName = prompt("Please enter your fullname:");
		let myAge = prompt("Please enter your age:");
		let address = prompt("Please enter your address:");
		console.log(fullName);
		console.log(myAge);
		console.log(address);
		alert("Thank you!");
	}

	printBio();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printBands() {
		console.log("1. Cigarettes After Sex");
		console.log("2. Bee Gees");
		console.log("3. Silk");
		console.log("4. Ben & Ben");
		console.log("5. The Chain Smokers");		
	};

	printBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printMovies() {
		let movie1Rating = ("Rotten tomatoes rating: 85%");
		let movie2Rating = ("Rotten tomatoes rating: 96%");
		let movie3Rating = ("Rotten tomatoes rating: 85%");
		let movie4Rating = ("Rotten tomatoes rating: 83%");
		let movie5Rating = ("Rotten tomatoes rating: 74%");
		let movie1 = "Man from Earth";
		let movie2 = "Fight Club";
		let movie3 = "American Psycho";
		let movie4 = "The Wolf of Wallstreet";
		let movie5 = "Limitless";
		console.log("My top 5 favorite movies:");
		console.log(movie1);
		console.log(movie1Rating);
		console.log(movie2);
		console.log(movie2Rating);
		console.log(movie3);
		console.log(movie3Rating);
		console.log(movie4);
		console.log(movie4Rating);
		console.log(movie5);
		console.log(movie5Rating);
		alert("Classy!");
	}

	printMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



function printFriends() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);

};

printFriends();
